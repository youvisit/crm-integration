const { createDb, createRedisCache } = require('./db');
const { db_config, redis_config, authorizer_function, server_port } = require('./util');
const makeDependencies = require('./dependencies');
const { generateRoutes } = require('./routes');
const main = async () => {
    const db = createDb(db_config);
    const redis = createRedisCache(redis_config);
    const dependencies = await makeDependencies({
        dbContext: db,
        redisContext: redis,
        JWT_LAMBDA_FN: authorizer_function,
    });
    const app = generateRoutes(dependencies);
    app.listen(server_port, () => console.log('LISTENING ON PORT 8000'));
};

main();
