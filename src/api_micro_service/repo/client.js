class ClientRepository {
	constructor(clientModel, slateIntegrationModel) {
		this.clientModel = clientModel;
		this.slateIntegrationModel = slateIntegrationModel;
	}
	findAll() {
		return this.clientModel
			.findAll({
				include: [
					{
						model: this.slateIntegrationModel,
					},
				],
			})
			.then((data) => data);
	}
	findById(id) {
		return this.clientModel
			.findOne({
				where: {
					id: id,
				},
				include: [{ model: this.slateIntegrationModel }],
			})
			.then((data) => data);
	}
	create(clientModel) {
		return this.clientModel
			.create(clientModel, {
				include: [{ model: this.slateIntegrationModel }],
			})
			.then((data) => data);
	}
}

module.exports = { ClientRepository };
