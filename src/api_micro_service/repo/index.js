const { ClientRepository } = require('./client');
const { AudienceProfileRepository } = require('./audienceProfile');
const { JWTSecretsRepo } = require('./jwtSecretsRepo');
module.exports = {
    ClientRepository,
    AudienceProfileRepository,
    JWTSecretsRepo,
};
