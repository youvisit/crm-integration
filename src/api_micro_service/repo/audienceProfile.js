class AudienceProfileRepository {
	constructor(graphQLClient) {
		this.graphQLClient = graphQLClient;
	}
	findAudienceProfilesByInstIdWithPageableSpec(authToken, instId, { page = 1, size = 100 }) {
		return this.graphQLClient
			.query(
				`query($inst_id:String!,$page:Int,$size:Int){
            audiences(inst_id: $inst_id, page: $page, size: $size) {
              page
              totalPages
              totalRecords
              recordsPerPage
              prevPage
              nextPage
              profiles {
                firstName
                lastName
                email
                enrollTerm
                gender
                graduationYear
              }
            }
          }
          `,
				{
					inst_id: instId,
					page: page,
					size: size,
				},
				authToken
			)
			.then((data) => data)
			.catch((err) => console.log(err));
	}
}

module.exports = {
	AudienceProfileRepository,
};
