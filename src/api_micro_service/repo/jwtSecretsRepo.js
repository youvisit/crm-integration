class JWTSecretsRepo {
    static TTL_JWT = 60 * 30; // CACHE TIME HALF AN HOUR
    constructor(redisClient) {
        this.prefix = 'JWT_SECRETS';
        this.redisClient = redisClient;
    }
    createSecretCache = (instId, jwtToken) => {
        return this.redisClient.set(this.prefix + '_' + instId, jwtToken, JWTSecretsRepo.TTL_JWT);
    };
    findTokenByInstituion = (instId) => {
        return this.redisClient.get(this.prefix + '_' + instId);
    };
}
module.exports = { JWTSecretsRepo };
