module.exports = {
	BaseResponse: ({ data, isSuccess = true, rowsModified = 0, message = 'OK' }) => ({
		data,
		isSuccess,
		rowsModified,
		message,
	}),
};
