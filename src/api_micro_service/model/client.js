const Sequelize  = require('sequelize');
module.exports = (dbContext) => {
	return dbContext.define('clients', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		createdBy: {
			type: Sequelize.STRING,
		},
		instId: {
			type: Sequelize.STRING,
		},
		frequency: {
			type: Sequelize.ENUM,
			values: ['WEEKLY', 'MONTHLY', 'DAILY'],
		},
	});
};
