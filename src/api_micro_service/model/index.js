const createClient = require('./client');
const createSlateIntegration = require('./slateIntegration');

module.exports = {
	setupModels: async (dbContext) => {
		const Client = createClient(dbContext);
		const SlateIntegration = createSlateIntegration(dbContext);
		Client.hasOne(SlateIntegration);
		await Client.sync();
		await SlateIntegration.sync();
		return {
			Client,
			SlateIntegration,
		};
	},
};
