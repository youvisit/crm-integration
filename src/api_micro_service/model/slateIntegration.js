const Sequelize = require('sequelize');
module.exports = (dbContext) => {
	const SlateIntegraiton = dbContext.define('slateIntegration', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
		},
		docType: {
			type: Sequelize.ENUM,
			values: ['JSON', 'XML', 'CSV'],
		},
		frequency: {
			type: Sequelize.ENUM,
			values: ['WEEKLY', 'MONTHLY', 'DAILY'],
		},
		jobSchema: {
			type: Sequelize.TEXT('long'),
		},
		agentUserName: {
			type: Sequelize.STRING,
		},
		agentPassword: {
			type: Sequelize.STRING,
		},
		endpoint: {
			type: Sequelize.TEXT('medium'),
		},
	});
	return SlateIntegraiton;
};
