const { BaseResponse } = require('../dto');
class ClientController {
    constructor(
        clientRepo,
        slateIntegrationRepo,
        audienceProfileRepo,
        jwtAuthorizer,
        jwtSecretsRepo
    ) {
        this.clientRepo = clientRepo;
        this.slateIntegrationRepo = slateIntegrationRepo;
        this.audienceProfileRepo = audienceProfileRepo;
        this.jwtAuthorizer = jwtAuthorizer;
        this.jwtSecretsRepo = jwtSecretsRepo;
    }
    createClient = async (req, res) => {
        try {
            const response = await this.clientRepo.create({
                instId: req.body.instId,
                frequency: req.body.frequency,
                slateIntegration: req.body.slateIntegration,
            });
            res.status(201).send(BaseResponse({ data: response }));
        } catch (err) {
            res.status(500).send(BaseResponse({ data: err, isSuccess: false }));
        }
    };
    getClient = async (req, res) => {
        try {
            const clients = await this.clientRepo.findAll();
            res.status(200).send(BaseResponse({ data: clients }));
        } catch (err) {
            res.status(500).send(BaseResponse({ data: err, isSuccess: false }));
        }
    };
    getClientById = async (req, res) => {
        const id = req.params.id;
        const response = await this.clientRepo.findById(id);
        if (response) {
            res.status(200).send(BaseResponse({ data: response }));
            return;
        }
        res.status(404).send(BaseResponse({ data: response, message: 'Not Found' }));
    };
    getClientInst = async (req, res) => {
        const id = req.params.id;
        const client = await this.clientRepo.findById(id);
        if (!client) {
            res.status(404).send(BaseResponse({ data: client, message: 'Not Found' }));
            return;
        }
        let token = await this.jwtSecretsRepo.findTokenByInstituion(client.id);
        if (!token) {
            token = await this.jwtAuthorizer.get(client.instId);
            await this.jwtSecretsRepo.createSecretCache(client.id, token, ClientController.TTL_JWT);
        }
        const profiles = await this.audienceProfileRepo.findAudienceProfilesByInstIdWithPageableSpec(
            token,
            client.instId,
            {
                size: 10,
            }
        );
        res.status(200).send(BaseResponse({ data: profiles }));
    };
}

module.exports = {
    ClientController,
};
