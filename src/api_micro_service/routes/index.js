const express = require('express');
const router = express();
const bodyparser = require('body-parser');

const generateRoutes = ({ controllers: controllerFacade, middleware }) => {
	router.use(bodyparser.json());
	//client configuration
	router.get('/clients', controllerFacade.getClient);
	router.get('/clients/:id', controllerFacade.getClientById);
	router.get('/clients/:id/student-audience-profiles', controllerFacade.getClientInst);
	router.post('/clients', controllerFacade.createClient);
	return router;
};

module.exports = {
	generateRoutes,
};
