const makeControllerFacade = require('./controller');
const makeRepository = require('./repository');
const { GraphQLYouvisitClient, JWTAuthorizationYouvisit } = require('../util');
const { setupModels } = require('../model');

module.exports = async ({ dbContext, redisContext, JWT_LAMBDA_FN }) => {
    const models = await setupModels(dbContext);
    const graphQLClient = new GraphQLYouvisitClient(
        'https://graphql.dev.youvisit.com',
        'dev',
        true
    );
	const jwtAuth = new JWTAuthorizationYouvisit(JWT_LAMBDA_FN);
    const repository = makeRepository(models, graphQLClient, redisContext);
    return {
        controllers: makeControllerFacade(repository, { jwtAuthorizer: jwtAuth }),
    };
};
