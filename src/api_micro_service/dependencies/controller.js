const { ClientController } = require('../controller');
module.exports = (
    { clientRepo, slateIntegrationRepo, audienceProfileRepo, jwtSecretRepo },
    { jwtAuthorizer }
) => {
    return {
        ...new ClientController(
            clientRepo,
            slateIntegrationRepo,
            audienceProfileRepo,
            jwtAuthorizer,
            jwtSecretRepo
        ),
    };
};
