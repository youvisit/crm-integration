const { ClientRepository, AudienceProfileRepository, JWTSecretsRepo } = require('../repo');
module.exports = (models, graphQLClient, redisClient) => ({
    clientRepo: new ClientRepository(models.Client, models.SlateIntegration),
    audienceProfileRepo: new AudienceProfileRepository(graphQLClient),
    jwtSecretRepo: new JWTSecretsRepo(redisClient),
});
