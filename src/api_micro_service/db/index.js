const Sequelize = require('sequelize');
const { RedisClient } = require('../util');

module.exports = {
    createDb: (connectionConfig) => new Sequelize.Sequelize(connectionConfig),
    createRedisCache: (connectionConfig) => new RedisClient(connectionConfig),
};
