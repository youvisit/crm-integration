const { Lambda } = require('aws-sdk');
module.exports = {
	lambdaAction: (funcName, body) => {
		return new Lambda({ region: 'us-east-1' })
			.invoke({
				FunctionName: funcName,
				Payload: JSON.stringify(body),
				InvocationType: 'RequestResponse',
			})
			.promise();
	},
};
