const axios = require('axios').default;
const request = axios.create({ timeout: 20 * 1000 });
class GraphQLYouvisitClient {
	constructor(endpoint, env, log = true) {
		this.endpoint = endpoint;
		this.env = env;
		this.log = log ? console.log : () => {};
	}
	query = (query, variables, bearerToken) => {
		this.log('GRAPHQL-REQUEST :  ->', query);
		return request
			.post(
				this.endpoint,
				{
					query: query,
					variables,
				},
				{
					headers: {
						env: this.env,
						Authorization: bearerToken,
					},
				}
			)
			.then(({ data }) => {
				return data.data;
			})
			.catch((err) => err);
	};
}

module.exports = {
	GraphQLYouvisitClient,
};
