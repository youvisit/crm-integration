const { GraphQLYouvisitClient } = require('./graphQL');
const { JWTAuthorizationYouvisit } = require('./jwtAuthorization');
const { RedisClient } = require('./redisClient');
module.exports = {
    db_config: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT,
        database: process.env.DB_DATABASE,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
    },
    redis_config: {
        port: process.env.REDIS_PORT,
        host: process.env.REDIS_HOST,
    },
    authorizer_function: process.env.JWT_LAMBDA_FN,
    server_port: process.env.SERVER_PORT,
    GraphQLYouvisitClient,
    JWTAuthorizationYouvisit,
    RedisClient,
};
