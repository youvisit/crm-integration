const { lambdaAction } = require('./lambdaAction');
class JWTAuthorizationYouvisit {
	constructor(lambdaAuthorizerName, subject = 'crm-integration-agent', expiry = 1800) {
		this.lambdaAuthorizerName = lambdaAuthorizerName;
		this.subject = subject;
		this.expiry = expiry;
	}
	get = async (instId) => {
		const token = await lambdaAction(this.lambdaAuthorizerName, {
			meta: { user_type: 'job-agent', inst_id: instId, subject: this.subject },
			expiry: this.expiry,
		}).then(({ Payload }) => `Bearer ${JSON.parse(Payload).body}`);
		return token;
	};
}

module.exports = {
	JWTAuthorizationYouvisit,
};
