const redis = require('redis-promisify');
class RedisClient {
    constructor(redisConfig) {
        this.client = redis.createClient(redisConfig);
        this.client.on('connect', () => {
            console.log('CONNECTED TO REDIS SUCCESSFULLY');
        });
    }
    set = (k, v, ttl = false) => {
        this.client.setAsync(k, v).then((data) => {
            if (ttl) {
                this.client.expireAsync(k, ttl).then((data) => {
                    console.log('expires', data);
                });
            }
        });
    };
    get = (k) => this.client.getAsync(k);
}

module.exports = { RedisClient };
